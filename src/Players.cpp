#include "Players.hpp"

//Player::Player(): got_possession_ts(INVALID_TS), possession_time(0.0f) {}

/*
Player::Player(std::string team, std::string name, std::unordered_set<int> sids):
				team(team), got_possession_ts(INVALID_TS), possession_time(0.0f), name(name), sids(sids) {

}
*/

Player::Player(std::string name, std::unordered_set<int> sids):
	team(NO_TEAM), got_possession_ts(INVALID_TS), possession_time(0.0f), name(name), sids(sids) {}


void Player::print() const{
	std::cout << "Player description: " << std::endl
			  << "Name: " << name << std::endl
			  << "Team: " << team << std::endl
			  << "Sensor ids: { ";
	for (int sid: sids) {
		std::cout << sid << " ";
	}
	std::cout << "}" << std::endl;
}

void Player::start_possession(unsigned long long ball_ts) {
	got_possession_ts = ball_ts;
}

void Player::end_possession(unsigned long long ball_ts, bool interruption_to_account, Interruption interr) {
	if (sids.find(NONE_PLAYER_SID) != sids.end()) {
//		std::cout << "Computing possession time of None Player. start_ts: " << got_possession_ts << ", end_ts: " << ball_ts << std::endl;
//		std::cout << "possession_time = " << possession_time << std::endl;
//		std::cout << "got_possession_ts = " << got_possession_ts << std::endl;
//		std::cout << "end_possessession (ball_ts) = " << ball_ts << std::endl;
//		std::cout << "interruption_begin = " << interr.get_begin() << std::endl;
//		std::cout << "interruption_end = " << interr.get_end() << std::endl;
	}

	unsigned long long to_subtract=0;
	if (interruption_to_account) {
		to_subtract = interr.get_end()-interr.get_begin();
	}
	if (got_possession_ts != INVALID_TS) {
		//std::cout << "possession_time: " << possession_time << std::endl;
		possession_time += ((static_cast<float>(ball_ts) - static_cast<float>(got_possession_ts)) - static_cast<float>(to_subtract))*1e-12;
		//std::cout << "possession_time: " << possession_time << std::endl;
		got_possession_ts=INVALID_TS;
	}

	if (possession_time <0) {
		for (int i=0; i<1000; i++) {
			std::cout << "-----------------------------" << std::endl;
			std::cout << "POSSESSION TIME NEGATIVE!!!" << std::endl;
			std::cout << "-----------------------------" << std::endl;
		}
		std::cout << "Name: " << name << std::endl;
		std::cout << "possession_time = " << possession_time << std::endl;
		std::cout << "got_possession_ts = " << got_possession_ts << std::endl;
		std::cout << "end_possessession (ball_ts) = " << ball_ts << std::endl;
		std::cout << "Interruption to account: " << interruption_to_account << std::endl;
		std::cout << "interruption_begin = " << interr.get_begin() << std::endl;
		std::cout << "interruption_end = " << interr.get_end() << std::endl;

	}
	/*
	 * 	else {
	 * 		Something is terribly wrong
	 *	}
	 */

}

std::unordered_set<int> Player::get_sids() {
	return sids;
}

float Player::get_possession_time() {
	return possession_time;
}

unsigned long long Player::get_got_possession_ts() {
	return got_possession_ts;
}

//
// class Referee: public Player {
//   public:
//     bool isReferee = true;
//     Referee();
// };
//
// class Team {
//   public:
//     vector<Player> players;
//     Goal goal;
//     Team();
// };
