/*
 * GameState.cpp
 *
 *  Created on: 22 ago 2018
 *      Author: cataldocalo
 */

#include "GameState.hpp"

GameState::GameState(): pl_with_poss_sid(NONE_PLAYER_SID), pending_interruption(false),
						interrupted_game(false), in_game(false), finished_game(false) {

	// Create special "None player"
	std::unordered_set<int> sids ( {NONE_PLAYER_SID} );
	add_player("None player", sids);
	incr_sid_to_sid[0] = NONE_PLAYER_SID;
	sid_to_incr_sid[NONE_PLAYER_SID] = 0;
	players[NONE_PLAYER_SID]->start_possession(START_GAME_1ST_HALF_TS);

}

GameState::~GameState() {
	for (std::pair<std::string, Player*> element: name_to_player) {
		delete element.second;
	}
}

void GameState::print_int() {
	std::cout << "Now last_interruption = (" << last_interruption.get_begin() << ", " << last_interruption.get_end() << ")" << std::endl;
}


void GameState::add_player(std::string name, std::unordered_set<int> sids) {
	//Create an entry into the map sid->Player so that sids refer
	//to the same reference of Player who wears them
	Player *p = new Player(name, sids);
	for (int sid: sids) {
		players[sid] = p;
	}

	name_to_player[name] = p;
}

void GameState::add_team(std::string player_name, std::string team) {
	name_to_player[player_name]->team=team;

}

void GameState::add_players(std::unordered_map<std::string, std::unordered_set<int>> players) {
	int incr_sid=1;
	for (std::pair<std::string, std::unordered_set<int>> element: players) {
		// Populate map incr_sid (artifical) to sensor ID
		for (int sid : element.second) {
			incr_sid_to_sid[incr_sid] = sid;
			sid_to_incr_sid[sid] = incr_sid;
			incr_sid++;
		}
		add_player(element.first, element.second);
	}
	initialize_distances();
}

void GameState::initialize_distances() {
	sq_distances.reserve(this->players.size());
	for (int i=0; i<players.size(); i++) {
		sq_distances.push_back(0.0f);
	}
}

void GameState::add_teams(std::unordered_map<std::string, std::string> teams) {
	for (std::pair<std::string, std::string> name_team: teams) {
		add_team(name_team.first, name_team.second);
	}
}

void GameState::add_balls(std::unordered_set<int> balls_sids) {
	for (int sid: balls_sids) {
		add_ball(sid);
	}

}

void GameState::add_interr_begins(std::unordered_set<int> interr_begins_sids) {
	for (int sid: interr_begins_sids) {
		interr_begins.insert(sid);
	}
}

void GameState::add_interr_ends(std::unordered_set<int> interr_ends_sids) {
	for (int sid: interr_ends_sids) {
		interr_ends.insert(sid);
	}
}

std::string GameState::get_player_name(int sid) {
	return players[sid]->name;
}

void GameState::add_ball(int sid) {
	balls_positions[sid] = Position::get_out_of_field_position();
}

bool GameState::is_player(int sid) {
	return players.find(sid) != players.end();
}

bool GameState::is_ball(int sid) {
	return balls_positions.find(sid) != balls_positions.end();
}

bool GameState::is_interr_begin(int sid) {
	return interr_begins.find(sid) != interr_begins.end();
}

bool GameState::is_interr_end(int sid) {
	return interr_ends.find(sid) != interr_ends.end();
}

bool GameState::is_game_finished() {
	return finished_game;
}


bool GameState::is_in_field(Position pos) {
	return (pos.get_x()>=FIELD_LOWER_X && pos.get_x()<=FIELD_UPPER_X && pos.get_y()>=FIELD_LOWER_Y && pos.get_y()<=FIELD_UPPER_Y);
}

int GameState::process_sensor_event(Event e) {
	////std::cout << "Updating position of sensor with ID " << e.sid << std::endl;

	process_running_state(e.ts);

	int event_type;
	if (is_ball(e.sid)) {
		event_type = BALL_EVENT;
		balls_positions[e.sid] = e.get_position();
		if (is_in_field(e.get_position())) {
			active_ball_sid = e.sid;
		}
	}
	else if (is_player(e.sid)) {
		players_positions[e.sid] = e.get_position();
		event_type = PLAYER_EVENT;
	}
	else if (is_interr_begin(e.sid)) {
		//std::cout << "Found an interruption begin event!" << std::endl;
		event_type = INTERR_BEGIN_EVENT;
		set_interruption_begin(e.ts);
	}
	else if (is_interr_end(e.sid)) {
		//std::cout << "Found an interruption end event!" << std::endl;
		event_type = INTERR_END_EVENT;
		set_interruption_end(e.ts);
	}

	return event_type;
}

void GameState::process_running_state(unsigned long long current_ts) {

	if (current_ts>=TECH_ISSUE_BEGIN_TS && current_ts<=TECH_ISSUE_END_TS) {
		if (during_tech_issue && pl_with_poss_sid != INVALID_SID) {
			players[pl_with_poss_sid]->end_possession(END_GAME_1ST_HALF_TS, pending_interruption, last_interruption);
			pl_with_poss_sid = INVALID_SID;
		}
		during_tech_issue=true;
	}
	else {
		during_tech_issue=false;
	}


	if ((current_ts>=START_GAME_1ST_HALF_TS && current_ts<= END_GAME_1ST_HALF_TS) 			//1st half of the game
		|| (current_ts>=START_GAME_2ND_HALF_TS && current_ts<= END_GAME_2ND_HALF_TS)) {		//2nd half of the game

		in_game=true;
	}
	else {
		if (in_game && 										//Game was going on up to now
			(current_ts>=END_GAME_1ST_HALF_TS && current_ts<=START_GAME_2ND_HALF_TS) ) {		//and now we are during game interval
			// End possession of the ball for the player who had it up to now
			if (pl_with_poss_sid != INVALID_SID) {
				players[pl_with_poss_sid]->end_possession(END_GAME_1ST_HALF_TS, pending_interruption, last_interruption);
				pl_with_poss_sid = INVALID_SID;
			}
			// Print that 1st half of the game is finished
			/*
			std::cout << "---------------------------" << std::endl;
			std::cout << "1ST HALF OF THE GAME IS FINISHED" << std::endl;
			std::cout << "---------------------------" << std::endl;
			*/

		}
		else if (in_game &&								//Game was going on up to now
				current_ts>=END_GAME_2ND_HALF_TS) {											//and now game is finished
			// End possession of the ball for the player who had it up to now
			if (pl_with_poss_sid != INVALID_SID) {
				players[pl_with_poss_sid]->end_possession(END_GAME_2ND_HALF_TS, pending_interruption, last_interruption);
				pl_with_poss_sid = INVALID_SID;
			}
			/*
			std::cout << "---------------------------" << std::endl;
			std::cout << "2ND HALF OF THE GAME IS FINISHED" << std::endl;
			std::cout << "---------------------------" << std::endl;
			*/
			finished_game=true;
		}
		//Anyway now game is not running
		in_game = false;
	}
}

void GameState::compute_distances() {
	//std::cout << "Computing distances..." << std::endl;

	//if (players_positions.find(NONE_PLAYER_SID) != players_positions.end()) {
	//	std::cout << "players_positions contains sid = " << NONE_PLAYER_SID << std::endl;
	//}

	// Distance of NONE_PLAYER is always infinite
	sq_distances[0] = std::numeric_limits<float>::max();
	//std::cout << "(sid, distance): (" << get_sid(0) << "," << sq_distances[0] << ")" << std::endl;

	Position ball_position;

	# pragma omp parallel for shared(sq_distances, players_positions, incr_sid_to_sid) private(ball_position) schedule(static, 2)
	for (int i=1; i<sq_distances.size(); i++) {
		//std::cout << "Number of threads:" << omp_get_num_threads() << std::endl;
		ball_position = balls_positions[active_ball_sid];
		int sid = get_sid(i);
		sq_distances[i] = players_positions[sid].sq_distance(ball_position);
		//std::cout << "(sid, distance): (" << sid << "," << sq_distances[i] << ")" << std::endl;
	}

	/*
	for (std::pair<int, Position> pair: players_positions) {
			distances[pair.first] = ball_position.sq_distance(pair.second);
			//std::cout << "(sid, distance): (" << pair.first << "," << distances[pair.first] << ")" << std::endl;
	}
	*/

}

void GameState::print_distances() {

	std::cout << "-----------------------------------------" << std::endl;
	std::cout << "Distances: size " << sq_distances.size() << std::endl;
	std::cout << "-----------------------------------------" << std::endl;
	for (int i=0; i<sq_distances.size(); i++) {
		std::cout << "(" << get_sid(i) << ", " << sq_distances[i] << ")" << std::endl;
	}
	std::cout << "-----------------------------------------" << std::endl;

}

std::pair<int, float> GameState::get_player_with_ball_poss_NEW() {
	    std::vector<float>::iterator result = std::min_element(std::begin(sq_distances), std::end(sq_distances));
	    int index = get_sid( std::distance(std::begin(sq_distances), result) );
	    float min_distance = *result;
	    //std::cout << "(index, min_distance) = (" << index << ", " << min_distance << ")" << std::endl;
	    return std::make_pair(index, min_distance);
}

std::pair<int, float> GameState::get_player_with_ball_poss()
{
	// Find the pair with minimum value in the map <sid, distance>
	using pair_type = decltype(distances)::value_type;

	auto pr = std::min_element
	(
	    std::begin(distances), std::end(distances),
	    [] (const pair_type & p1, const pair_type & p2) {
	        return p1.second < p2.second;
	    }
	);
	return std::make_pair(pr->first, pr->second);
}

void GameState::update_possession(int new_possessor_sid, unsigned long long ball_ts) {

	// If the player already had the possession, do not do anything
	// Otherwise update the time accumulator of previous player with possession
	// and update the timestamp of the start of the possessor
	//std::cout << "CONDITION (new_possessor_sid != pl_with_poss_sid): " << ((new_possessor_sid != pl_with_poss_sid) ? "true" : "false") << std::endl;

	if (new_possessor_sid != pl_with_poss_sid) {
		//std::cout << "pl_with_poss_sid: " << pl_with_poss_sid << " ";
		//std::cout << players[pl_with_poss_sid]->name << "got possession at " << players[pl_with_poss_sid]->get_got_possession_ts() << ", ended at " << ball_ts << std::endl;

		if (pl_with_poss_sid != INVALID_SID)
			players[pl_with_poss_sid]->end_possession(ball_ts, pending_interruption, last_interruption);

		if (pending_interruption) {
			//std::cout << "Taking into account interruption (" << last_interruption.get_begin() << ", " << last_interruption.get_end() << ") into possession time..." << std::endl;
			reset_interruption();
		}
		// Just some prints for debugging
		// -----------------------------------------------
//		std::unordered_set<int> old_sids = players[pl_with_poss_sid]->get_sids();
//		std::cout << players[pl_with_poss_sid]->name << " { ";
//		for (int sid: old_sids) {
//			std::cout << sid << " ";
//		}
//		std::cout << "} has not the ball anymore. Time possession: " << players[pl_with_poss_sid]->get_possession_time() << " sec." << std::endl;
//		 //-----------------------------------------------


		players[new_possessor_sid]->start_possession(ball_ts);


//		 //Just for some prints for debugging
//		 //-----------------------------------------------
//		std::unordered_set<int> new_sids = players[new_possessor_sid]->get_sids();
//
//		std::cout << players[new_possessor_sid]->name << " { ";
//		for (int sid: new_sids) {
//			std::cout << sid << " ";
//		}
//		std::cout <<"} has the ball now." << std::endl;
//		 //-----------------------------------------------

		pl_with_poss_sid=new_possessor_sid;
	}

}

void GameState::set_interruption_begin(unsigned long long begin_ts) {
	interrupted_game=true;
	last_interruption.set_begin_ts(begin_ts);
	last_interruption.set_end_ts(INVALID_TS);

	//print_int();
}

void GameState::set_interruption_end(unsigned long long end_ts) {
	//std::cout << "Setting interruption end..." << std::endl;
	interrupted_game=false;
	last_interruption.set_end_ts(end_ts);

	pending_interruption=true;
	//print_int();
}

void GameState::reset_interruption() {
	pending_interruption=false;
	//std::cout << "last_interruption: " << last_interruption.get_begin() << ", " << last_interruption.get_end() << std::endl;
	last_interruption.set_begin_ts(INVALID_TS);
	last_interruption.set_end_ts(INVALID_TS);
	//print_int();
}

bool GameState::is_game_active() {
	return in_game;
}

bool GameState::is_game_interrupted() {
	return interrupted_game;
}

bool GameState::is_during_tech_issue() {
	return during_tech_issue;
}

std::unordered_map<int, Player*> GameState::get_sid_to_players(){
	return players;
}

std::map<std::string, Player*> GameState::get_name_to_player(){
	return name_to_player;
}

int GameState::get_sid(int incr_id) {
	return incr_sid_to_sid[incr_id];
}


void GameState::print() {
	//std::cout << "-------------------------------------" << std::endl;
	//std::cout << "\tPLAYERS\t" << std::endl;
	//std::cout << "-------------------------------------" << std::endl;

    for (std::pair<std::string, const Player*> element: name_to_player) {
    	element.second->print();
    	//std::cout << "-------------------------------------" << std::endl;
    }

    //std::cout << "\tBALLS\t" << std::endl;
    //std::cout << "-------------------------------------" << std::endl;

    for (std::pair<int, Position> pair: balls_positions) {
    	//std::cout << pair.first << " ";
    }
    //std::cout << std::endl << "-------------------------------------" << std::endl;

    //std::cout << "\tINTERRUPTION BEGINS\t" << std::endl;
    //std::cout << "-------------------------------------" << std::endl;

    for (int sid: interr_begins) {
    	//std::cout << sid << " ";
    }
    //std::cout << std::endl << "-------------------------------------" << std::endl;

    //std::cout << "\tINTERRUPTION ENDS\t" << std::endl;
    //std::cout << "-------------------------------------" << std::endl;

    for (int sid: interr_ends) {
    	//std::cout << sid << " ";
    }

    //std::cout << std::endl << "-------------------------------------" << std::endl;
}

void GameState::print_sid_maps() {
	std::cout << "-------------------------------------" << std::endl;
	std::cout << "FIRST MAP (incr_sid, sid):" << std::endl;
	for (std::pair<int, int> pair : incr_sid_to_sid) {
		std::cout << "(" << pair.first << ", " << pair.second << ")" << std::endl;
	}
	std::cout << "-------------------------------------" << std::endl;
	std::cout << "SECOND MAP (sid, incr_sid):" << std::endl;
	for (std::pair<int, int> pair : sid_to_incr_sid) {
		std::cout << "(" << pair.first << ", " << pair.second << ")" << std::endl;
	}
	std::cout << "-------------------------------------" << std::endl;
}

std::unordered_map<int, int> GameState::get_sid_to_incr_sid() {
	return sid_to_incr_sid;
}

