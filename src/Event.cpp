/*
 * Event.cpp
 *
 *  Created on: 19 ago 2018
 *      Author: cataldocalo
 */

#include "Event.hpp"

Event::Event(int sid, unsigned long long ts, int x, int y, int z):
		  sid(sid), ts(ts), x(x), y(y), z(z) {}

Event::Event(int sid, unsigned long long ts, const Position& pos): Event(sid, ts, pos.get_x(), pos.get_y(), pos.get_z()) {}

Event::Event(const Event& e):Event(e.sid, e.ts, e.x, e.y, e.z) {}

Position Event::get_position() {
	Position p(x,y,z);
	return p;
}

void Event::print() {
	printf("%d\t%llu\t%d\t%d\t%d\n", sid, ts, x, y, z);
}



