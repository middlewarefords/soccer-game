/*
 * SpecializedEventParser.cpp
 *
 *  Created on: 27 ago 2018
 *      Author: cataldocalo
 */

#include "Event.hpp"
#include "SpecializedEventParser.hpp"

#include <vector>


SpecializedEventParser::SpecializedEventParser(std::string game_data_path, int T, unsigned long long start_of_the_game_ts)
	 : EventParser(game_data_path, T, start_of_the_game_ts) {
	this->batch_count=0;

}

std::vector<Event> SpecializedEventParser::parse_window_of_event(bool& last_batch) {

	std::vector<Event> vec_ret = EventParser::parse_window_of_event(last_batch);
	this->batch_count++;
	return vec_ret;
}


