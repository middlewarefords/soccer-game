/*
 * MetadataParser.cpp
 *
 *  Created on: 21 ago 2018
 *      Author: cataldocalo
 */

#include "MetadataParser.hpp"

MetadataParser::MetadataParser(std::string sensors_file_path, std::string first_half, std::string second_half,
							   unsigned long long offset_1st_half, unsigned long long offset_2nd_half):
	sensors_file_path(sensors_file_path), int_fir_half_path(first_half),
	int_sec_half_path(second_half), offset_1st_half(offset_1st_half), offset_2nd_half(offset_2nd_half),
	sensors_file_parsed(false), interr_1sthalf_file_parsed(false), interr_2ndhalf_file_parsed(false) {

	//cout << "Creating MetaParser object..." << endl;
}

std::unordered_map<std::string, std::unordered_set<int>> MetadataParser::parse_players() {
	if (!sensors_file_parsed) {
		parse_sensors_file();
	}

	return players;
}

std::unordered_map<std::string, std::string> MetadataParser::parse_teams() {
	if (!sensors_file_parsed) {
		parse_sensors_file();
	}

	return teams;
}

std::unordered_set<int> MetadataParser::parse_balls() {
	if (!sensors_file_parsed) {
		parse_sensors_file();
	}

	return balls;
}

std::unordered_set<int> MetadataParser::get_interr_begin_sids() {
	if (!interr_1sthalf_file_parsed || !interr_2ndhalf_file_parsed) {
		parse_interruptions();
	}
	return interr_begin;
}

std::unordered_set<int> MetadataParser::get_interr_end_sids() {
	if (!interr_1sthalf_file_parsed || !interr_2ndhalf_file_parsed) {
		parse_interruptions();
	}
	return interr_end;
}

void MetadataParser::parse_sensors_file() {

	players.clear();
	balls.clear();
	sensors_file_parsed=false;

	std::ifstream fs(sensors_file_path);

	std::string s;
	std::smatch m;
	std::string team; // Team A is true and Team B is false

	std::regex t_regex("Team (\\w)");
	std::regex p_regex("(\\w+\\s\\w+)\\s\\(Left\\sLeg:\\s(\\d{2,3}),\\sRight\\sLeg:\\s?(\\d{2,3})\\)");
	std::regex g_regex("(\\w+\\s\\w+)\\s\\(Left Leg: (\\d{2,3}), Right Leg: (\\d{2,3}), Left Arm: (\\d{2,3}), Right Arm: (\\d{2,3})\\)");
	std::regex r_regex("Referee \\(Left Leg: (\\d{2,3}), Right Leg: (\\d{2,3})\\)");
	std::regex b1_regex("-1st\\sHalf:\\s(\\d{1,3}),\\s(\\d{1,3}),\\s(\\d{1,3})");
	std::regex b2_regex("-\\s2nd\\sHalf:\\s(\\d{1,3}),\\s(\\d{1,3}),\\s(\\d{1,3}),\\s(\\d{1,3})");

	while (std::getline(fs,s)) {
		if(std::regex_search(s,m,t_regex)) {              // Team change
			//cout << m[1] << " Team" << endl;
			team = m[1];
		}
		else if(std::regex_search(s,m,p_regex)) {         // Player
			//cout << m[1] << " - " << m[2] << " - " << m[3] << endl;

			std::unordered_set<int> sids ( {std::stoi(m[2]), std::stoi(m[3])} );
			players[m[1]] = sids;

			//std::cout << "Added entry <player_name, team> = <" << m[1] << ", " << team << "> to team map" << std::endl;
			teams[m[1]] = team;
		}
		else if(std::regex_search(s,m,g_regex)) {         // Goal
			//cout << m[1] << " - " << m[2] << " - " << m[3] << " - " << m[4] << " - " << m[5] << endl;
			std::unordered_set<int> sids ( {std::stoi(m[2]), std::stoi(m[3]), std::stoi(m[4]), std::stoi(m[5])} );
			players[m[1]] = sids;

			teams[m[1]] = team;
		}
		else if(std::regex_search(s, m, b1_regex)) {
			balls.insert(std::stoi(m[1]));
			balls.insert(std::stoi(m[2]));
			balls.insert(std::stoi(m[3]));
			//cout << "Balls: " << m[1] << " - " << m[2] << " - " << m[3] << endl;
		}
		else if(std::regex_search(s, m, b2_regex)) {
			balls.insert(std::stoi(m[1]));
			balls.insert(std::stoi(m[2]));
			balls.insert(std::stoi(m[3]));
			balls.insert(std::stoi(m[4]));
			//cout << "Balls: " << m[1] << " - " << m[2] << " - " << m[3] << " - " << m[4] << endl;
		}
		else if(std::regex_search(s,m,r_regex)) {       // Referee
			//cout << "Referee" << " - " << m[1] << " - " << m[2] << endl;
		}
	}

	fs.close();

	sensors_file_parsed=true;

	//cout << "Parsed " << players.size() << " players" << endl;
}

void MetadataParser::parse_interruptions_file(std::string interruption_file_path, unsigned long long offset_ts) {
	std::ifstream fs(interruption_file_path);

	std::string s;
	std::smatch m;


	std::regex beg_regex("(\\d+);Game\\sInterruption\\sBegin;(\\d\\d):(\\d\\d):(\\d\\d)\\.(\\d\\d\\d);\\w+");
	std::regex beg_first_line_regex("(\\d+);Game\\sInterruption\\sBegin;(\\d+);\\w+");
	std::regex end_regex("(\\d+);Game\\sInterruption\\sEnd;(\\d\\d):(\\d\\d):(\\d\\d)\\.(\\d\\d\\d);\\w+");


	while (std::getline(fs,s)) {

		if(std::regex_search(s,m,beg_regex)) {
			int sid = std::stoi(m[1]);
			unsigned long long ts = offset_ts + TO_PICO_SECONDS(std::stoi(m[2]), std::stoi(m[3]), std::stoi(m[4]), std::stoi(m[5]));
			interruptions.push(std::make_pair(sid, ts));

			interr_begin.insert(sid);
			//std::cout << "Begin found: <" << sid << ", " << ts << ">" << std::endl;
		}
		else if(std::regex_search(s,m,end_regex)) {
			int sid = std::stoi(m[1]);
			unsigned long long ts = offset_ts + TO_PICO_SECONDS(std::stoi(m[2]), std::stoi(m[3]), std::stoi(m[4]), std::stoi(m[5]));
			//cout << "End found: <" << sid << ", " << ts << ">" << endl;
			interruptions.push(std::make_pair(sid, ts));

			interr_end.insert(sid);
			//std::cout << "End found: <" << sid << ", " << ts << ">" << std::endl;

		}
		else if(std::regex_search(s,m,beg_first_line_regex)) {
			int sid = std::stoi(m[1]);
			unsigned long long ts = offset_ts + TO_PICO_SECONDS(std::stoi(m[2]), 0, 0, 0);
			interruptions.push(std::make_pair(sid, ts));

			interr_begin.insert(sid);

			//std::cout << "Begin found: <" << sid << ", " << ts << ">" << std::endl;
		}
		//else {
		//	cout << "Nope: " << s << endl;
		//}
	}

	fs.close();

}

std::queue<std::pair<int, unsigned long long>> MetadataParser::parse_interruptions() {

	// First parse file of the 1st half of the game
	if (!interr_1sthalf_file_parsed) {
		parse_interruptions_file(int_fir_half_path, offset_1st_half);
		interr_1sthalf_file_parsed=true;
	}

	// Then parse file of the 2nd half of the game
	if (!interr_2ndhalf_file_parsed) {
		parse_interruptions_file(int_sec_half_path, offset_2nd_half);
		interr_2ndhalf_file_parsed=true;
	}

	return interruptions;

}
