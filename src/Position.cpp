/*
 * Position.cpp
 *
 *  Created on: 19 ago 2018
 *      Author: cataldocalo
 */

#include "Position.hpp"

Position::Position(): x(FIELD_LOWER_X-50), y(FIELD_LOWER_Y-50), z(0) {

}

Position::Position(int x, int y, int z): x(x), y(y), z(z) {}

Position::Position(const Position& p):Position(p.x, p.y, p.z) {}


Position& Position::operator= (const Position& p) {
	if (this!=&p) {
		x = p.x;
		y = p.y;
		z = p.z;
	}
	return *this;
}

float Position::sq_distance(const Position& p) const {
	return static_cast<float>(x-p.x)*(x-p.x) + static_cast<float>(y-p.y)*(y-p.y) + static_cast<float>(z-p.z)*(z-p.z);
}

int Position::get_x() const {
	return x;
}

int Position::get_y() const {
	return y;
}

int Position::get_z() const {
	return z;
}
