#include "mpi.h"
#include <iostream>
#include <sstream>
#include <fstream>
#include <string>


#include "MonitorController.hpp"

#define MIN_K 1
#define MAX_K 5
#define MIN_T 1
#define MAX_T 60

void print_error_msg(int err, std::string msg)
{
    std::cerr << msg;
}

void read_input_parameters(int argc, char *argv[], int* k, int* t) {
	// Expected 3 arguments: program name, K and T
	// Optional 4th argument: nrows to print, only for debug
	if (argc < 3)  {
		std::stringstream msg;
		msg << "Usage: " << argv[0] << " K T" << std::endl;
		print_error_msg(1, msg.str());
	}

	// Input check and converting K and T to integer
	std::istringstream ss_k(argv[1]);
	std::istringstream ss_t(argv[2]);

	if (!(ss_k >> *k)) {
		std::stringstream msg;
		msg << "Invalid number: " << argv[1] << std::endl;
		print_error_msg(1, msg.str());
	}
	else if (!ss_k.eof()) {
		std::stringstream msg;
		msg << "Trailing characters after number: " << argv[1] << std::endl;
		print_error_msg(1, msg.str());
	}

	if (!(ss_t >> *t)) {
		std::stringstream msg;
		msg << "Invalid number: " << argv[2] << std::endl;
		print_error_msg(1, msg.str());
	}
	else if (!ss_t.eof()) {
		std::stringstream msg;
		msg << "Trailing characters after number: " << argv[1] << std::endl;
		print_error_msg(1, msg.str());
	}

}

void valid_k_t(int k, int t) {
	// Checking validity of K and T wrt respective ranges
	std::stringstream msg;
	if (!(k>=MIN_K && k<=MAX_K)) {
		msg << "Invalid value of K. It must hold " << MIN_K
			<< " <= K <= " << MAX_K << std::endl;
	}
	if(!(t>=MIN_T && t<=MAX_T)) {
		msg << "Invalid value of T. It must hold " << MIN_T
			<< " <= T <= " << MAX_T << std::endl;
	}
	if (!(k>=MIN_K && k<=MAX_K) || !(t>=MIN_T && t<=MAX_T)) {
		print_error_msg(1, msg.str());
	}
}


int main(int argc, char *argv[]) {
	int k, t;

    read_input_parameters(argc, argv, &k, &t);
    valid_k_t(k, t);
    std::cout << "Processing data with parameters: " << "K = " << k << "; T = " << t << std::endl;


    MonitorController mc;
    mc.run(t, k, "metadata.txt", "full-game", "referee-events/Game Interruption/1st Half.csv", "referee-events/Game Interruption/2nd Half.csv");

  return 0;
}
