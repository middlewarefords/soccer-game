#include "Utility.hpp"
#include "math.h"



unsigned long long TO_PICO_SECONDS(int hours, int minutes, int seconds, int milliseconds) {

	unsigned long long pico = (hours * 60 * 60 * 1000 +  	// Hours to ms
	           	   	   	   	  minutes * 60  * 1000 +      	// Minutes to ms
							  seconds * 1000 +           	// Seconds to ms
							  milliseconds) 				// Milliseconds
							  *1e+9;						// Milliseconds to ps
	return pico;

}
std::string TO_GAME_TIME(unsigned long long ts) {

	unsigned long long  ts_offset;
	float t;

	unsigned long long mm, ss, ms;

	unsigned long long timer_offset;
	std::string chrono;


	if (ts >= START_GAME_1ST_HALF_TS && ts <= END_GAME_1ST_HALF_TS ) { // If first period
		ts_offset = START_GAME_1ST_HALF_TS;
		timer_offset = 0;
	} else if (ts >= START_GAME_2ND_HALF_TS && ts <= END_GAME_2ND_HALF_TS) { // If second period
		ts_offset = START_GAME_2ND_HALF_TS;
		timer_offset = 30;
	} else {
		ts_offset = START_GAME_1ST_HALF_TS;
		timer_offset = 0;
		chrono = "OFF GAME ! ";
	}

	t = (ts - ts_offset)*1e-12;

	mm = t / 60;
	t = t - mm*60;
	ss = t;
	ms = (t - ss)*100;

	chrono = chrono + FORMAT_DIGIT(mm,2)+":"+FORMAT_DIGIT(ss,2)+"."+FORMAT_DIGIT(ms,3);

	return chrono;
}

//To string and adds leading zero
std::string FORMAT_DIGIT(int i, int l) {
	std::string d = std::to_string(i);
	if (d.length() == 1 && l == 2) {
		d = "0" + d;
	} else if (d.length() == 2 && l == 3) {
		d = "0" + d;
	} else if (d.length() == 1 && l == 3) {
		d = "00" + d;
	}
	return d;
}
