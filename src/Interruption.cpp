/*
 * Interruption.cpp
 *
 *  Created on: 01 set 2018
 *      Author: cataldocalo
 */

#include "Interruption.hpp"


Interruption::Interruption(unsigned long long begin_ts, unsigned long long end_ts): begin_ts(begin_ts), end_ts(end_ts) {}

Interruption::Interruption(): Interruption(INVALID_TS, INVALID_TS) {}

unsigned long long Interruption::get_begin() {
	return begin_ts;
}

void Interruption::set_begin_ts(unsigned long long begin_ts) {
	this->begin_ts = begin_ts;
}

unsigned long long Interruption::get_end() {
	return end_ts;
}

void Interruption::set_end_ts(unsigned long long end_ts) {
	this->end_ts = end_ts;
}
