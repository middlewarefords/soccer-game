#include "Statistics.hpp"

Statistics::Statistics(std::unordered_map<int, Player*> sid_to_players, std::unordered_map<int,int> sid_to_incr_sid,
		               std::string filename, int period): filename(filename), period(period),
					   T(1), console_out(false) {

	for (std::pair<int, Player*> sid_player : sid_to_players) {
		if (std::find(players.begin(), players.end(), sid_player.second) == players.end()) {
			players.push_back(sid_player.second);
			teams[sid_player.second->team].push_back(sid_player.second);
		}

	}

	file.open(filename);

	std::cout << "STATS CREATED" << std::endl;
}

/*
Statistics::Statistics(std::map<std::string, Player*> name_to_player, std::string filename, int period):
                          name_to_player(name_to_player), filename(filename), period(period) {

    zero_stats();

    for (std::pair<std::string, Player*> name_player: name_to_player) {
    	teams[name_player.second->team].push_back(name_player.second);
	}

    console_out = false;

    T = 1;

    file.open(filename); // Open in append mode

    std::cout << "STATS CREATED" << std::endl;
}
*/

Statistics::~Statistics() {
    if(file.is_open()) {
      file.close();
    }
}// Includes calculating and printing
void Statistics::print_statistics(bool game_finished) {

	//zero_stats();

	// Temp vars
	//Player* p;
	//std::string name;

	  for (std::pair<std::string, std::list<Player*>> team: teams) {
	    teams_possession[team.first] = 0.0f;
	  }
	// Loop to calculate the total possession time
	/*
	for ( auto it = name_to_player.begin(); it != name_to_player.end(); ++it ) {
		total_t += it->second->get_possession_time();
	}
	*/

	float total_t=0;
	#pragma omp parallel for shared(players) reduction(+: total_t)
	for (int i=0; i < players.size(); i++) {
		total_t += players[i]->get_possession_time();
	}

	// Loop to get possession for each player + team possession
	/*
	for ( auto it = name_to_player.begin(); it != name_to_player.end(); ++it ) {
		name = it->first;
		p = it->second;
		teams_possession[p->team] += p->get_possession_time();
		players_possession[p->name] = (p->get_possession_time()/total_t)*100;
	}
	*/
	float total;
	#pragma omp parallel for shared(players) private(total)
	for (int i=0; i < players.size(); i++) {
		total = total_t;
		Player* p = players[i];
		teams_possession[p->team] += p->get_possession_time();
		players_possession[p->name] = (p->get_possession_time()/total)*100;
	}

	// --------------------
	// Printing to file
	// --------------------

	print_to_file("---------------------------------------------");
	print_to_file("--------------------T="+ std::to_string(T) +"----------------------");
	print_to_file("---------------Elapsed time="+ std::to_string(T*period) +" sec----------------------");
	print_to_file("---------------------------------------------");


	for (std::pair<std::string, std::list<Player*>> team: teams) {
		print_to_file("\tTeam "+team.first+"\n" );
		for(Player* p : team.second) {
			print_to_file("\t\t"+p->name + " : "+ std::to_string(players_possession[p->name]) + "%");
		}
		print_to_file("\n" );
	}
	print_to_file("\n\tTotal possessions \n" );
	for (std::pair<std::string, float> team: teams_possession) {
		print_to_file("\t\tTeam "+team.first+" : "+std::to_string((team.second/total_t)*100)+"%");
	}
	print_to_file("\n");

	if (!game_finished)
		T++;
}


void Statistics::print_to_cout(std::string out) {
  std::cout << out << std::endl;
}

void Statistics::print_to_file(std::string out) {
  if (console_out) print_to_cout(out);
  if(!file.is_open()){
    std::cout << "!!! FILE IS NOT OPEN !!!" << std::endl; // Debug line
  }
  else {
    file << out + "\n";
  }
}

void Statistics::set_console_out(bool f) {
  console_out = f;
}

/*
void Statistics::zero_stats() {
  total_t = 0.0f;
  for (std::pair<std::string, Player*> e: name_to_player) {
    teams_possession[e.second->team] = 0.0f;
  }
}
*/

