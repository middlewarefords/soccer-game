/*
 * EventParser.cpp
 *
 *  Created on: 19 ago 2018
 *      Author: cataldocalo
 */

#include "EventParser.hpp"

EventParser::EventParser(std::string game_data_path, int T, unsigned long long start_of_the_game_ts):
	fs(game_data_path, std::ios::in), T(TO_PICO_SECONDS(0,0,T,0)), time_window_start(start_of_the_game_ts){
	//Skip header
	//printf("Building object: EventParser...\n");
	std::string s;
	getline(fs, s);
}

EventParser::~EventParser() {
	fs.close();
}

void EventParser::set_interruptions(std::queue<std::pair<int, unsigned long long>> interruptions) {
	this->interruptions = interruptions;
	Event e = get_current_interruption();
	//std::cout << "First interruption: (sid, ts) = (" << e.sid << ", " << e.ts << ")" << std::endl;
}

void EventParser::parse_next_event() {
	std::string s;
	if (getline(fs, s)) {
		const char* char_string = s.c_str();
		int sid, x, y, z;
		unsigned long long ts;

		// We are only interested in the first five terms of the event schema
		sscanf(char_string, "%d,%llu,%d,%d,%d",  &sid, &ts, &x, &y, &z);
		Event e(sid,ts,x,y,z);
		pending_events.push(e);
	}
	else {
		throw std::ios_base::failure{"Reached End Of File."};
	}
}

std::vector<Event> EventParser::parse_window_of_event(bool& last_batch) {

	last_batch=false;
	std::vector<Event> batch;
	batch.reserve(batch_size);

	unsigned long long current_ts = 0;

	//Read #batch_size events and push them on the vector batch
	//until batch_size is reached or time window T is finished
	//while (batch.size() < batch_size && current_ts < time_window_start+T && !is_eof_reached()) {
	while (batch.size() < batch_size && !(is_period_ended(current_ts)) && !is_eof_reached()) {
		try {
			parse_next_event();

			Event e = get_current_event();
			Event interruption = get_current_interruption();

			// If an interruption comes before the current event, first insert the interruption
			// and then go ahead
			if (interruption.ts != INVALID_TS && interruption.ts <= e.ts) {
				//std::cout << "Inserting interruption (" << interruption.sid << ", " << interruption.ts << ") into batch..." << std::endl;
				batch.push_back(interruption);
				advance_interr_head();
				current_ts = interruption.ts;
			}
			else {
				batch.push_back(e);
				advance_events_head();
				current_ts = e.ts;
			}


		}
		catch (std::ios_base::failure& ex) {

		}

	}


	/*
	if (current_ts >= time_window_start+T) {
		time_window_start = time_window_start+T;
		last_batch = true;
	}
	*/


	if (is_period_ended(current_ts)) {
		//std::cout << "Period is ended. Before update: time_window_start = " << time_window_start << std::endl;
		time_window_start = time_window_start+T;

		//if (during_game_interval(time_window_start))
		//	std::cout << "Removing interval= " << time_window_start << std::endl;

		while (during_game_interval(time_window_start)) {
			time_window_start += T;
		}
		last_batch = true
		//std::cout << "Period is ended. After update:  time_window_start = " << time_window_start << std::endl;
		//if (!during_game_interval(current_ts)) {
		;
		//}

	}


	return batch;

}

bool EventParser::is_period_ended(unsigned long long current_ts) {
	unsigned long long end_period_ts = time_window_start+T;
	while (during_game_interval(end_period_ts)) {
		end_period_ts += T;
	}

	/*
	if (current_ts >= end_period_ts) {
		std::cout << "----------------------------" << std::endl;
		std::cout << "current_ts =" << current_ts << std::endl;
		std::cout << "time_window_start =" << time_window_start << std::endl;
		std::cout << "----------------------------" << std::endl;
	}
	*/

	return current_ts >= end_period_ts; // + to_sum;

}

bool EventParser::is_eof_reached(){
	return fs.eof();
}

bool EventParser::during_game_interval(unsigned long long current_ts) {
	return (current_ts>=END_GAME_1ST_HALF_TS && current_ts<=START_GAME_2ND_HALF_TS);
}

Event EventParser::get_current_event() {
	if (!pending_events.empty()) {
		return pending_events.front();
	}
	Event e(INVALID_SID, INVALID_TS, Position::get_out_of_field_position());
	return e;
}

Event EventParser::get_current_interruption() {
	if (!interruptions.empty()) {
		std::pair<int, unsigned long long> interruption = interruptions.front();
		Event e(interruption.first, interruption.second, Position::get_out_of_field_position());
		return e;
	}

	Event e(INVALID_SID, INVALID_TS, Position::get_out_of_field_position());
	return e;
}

void EventParser::advance_events_head() {
	pending_events.pop();
}

void EventParser::advance_interr_head() {
	interruptions.pop();
}

