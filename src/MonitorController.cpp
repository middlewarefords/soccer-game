/*
 * MonitorController.cpp
 *
 *  Created on: 22 ago 2018
 *      Author: cataldocalo
 */

#include "MonitorController.hpp"
#include "SpecializedEventParser.hpp"
#include "Statistics.hpp"
#include "Utility.hpp"


void print_queue(std::queue<std::pair<int, unsigned long long>> q) {
	//std::cout << "-----------------------------------------" << std::endl;
	//std::cout << "\tINTERRUPTIONS\t" << std::endl;
	//std::cout << "-----------------------------------------" << std::endl;
	//while (!q.empty()) {
		//std::cout << "<sid, ts>: <" << q.front().first << ", " << q.front().second <<  "> " << std::endl;
	//	q.pop();
	//}
	//std::cout << std::endl;
	//std::cout << "-----------------------------------------" << std::endl;
}


void MonitorController::run(int T, int K, std::string meta_data_path, std::string game_data_path,
							std::string first_half_interruptions_file_path, std::string second_half_interruptions_file_path) {

/*
	MetadataParser mp(meta_data_path, first_half_interruptions_file_path, second_half_interruptions_file_path, START_GAME_1ST_HALF_TS, START_GAME_2ND_HALF_TS);
	std::queue<std::pair<int, unsigned long long>> interruptions = mp.parse_interruptions();

	//std::cout << "Queue: " << std::endl << "-------------------------------------" << std::endl;
	print_queue(interruptions);

	return;
	*/

	auto start = std::chrono::high_resolution_clock::now();
	GameState gs;
	EventParser ep(game_data_path, T, START_GAME_1ST_HALF_TS);

	// Initialize game state
	{
		// Read players and balls from metadata file and populate GameState
		std::unordered_map<std::string, std::unordered_set<int>> players;
		std::unordered_set<int> balls;
		std::unordered_map<std::string, std::string> teams;
		std::unordered_set<int> interr_begin_sids;
		std::unordered_set<int> interr_end_sids;
		std::queue<std::pair<int, unsigned long long>> interruptions_events;
		{
			MetadataParser mp(meta_data_path, first_half_interruptions_file_path, second_half_interruptions_file_path, START_GAME_1ST_HALF_TS, START_GAME_2ND_HALF_TS);
			players = mp.parse_players();
			teams = mp.parse_teams();
			balls = mp.parse_balls();
			interr_begin_sids = mp.get_interr_begin_sids();
			interr_end_sids = mp.get_interr_end_sids();
			interruptions_events = mp.parse_interruptions();
			//print_queue(interruptions_events);

		}
		gs.add_players(players);
		gs.add_teams(teams);
		gs.add_balls(balls);
		gs.add_interr_begins(interr_begin_sids);
		gs.add_interr_ends(interr_end_sids);
		ep.set_interruptions(interruptions_events);
	}

	//gs.print();

	int square_K = K*1000*K*1000;		// Meters to millimeters and then square it

	bool last_batch=false;

	Statistics statistics(gs.get_sid_to_players(), gs.get_sid_to_incr_sid(), "output.txt", T);
	//Statistics statistics(gs.get_name_to_player(), "output.txt", T);
	statistics.set_console_out(true);


	while(!gs.is_game_finished()) {

		//TODO remove this line of code after commit
		int in_game_batch = 2;//0000; //00000;
		int batch_count = 0;
		unsigned long long last_computation_ts;

		//while (batch_count < in_game_batch) {

			std::vector<Event> events_batch = ep.parse_window_of_event(last_batch);
			//std::cout << "Last batch " << last_batch << std::endl;

			//std::cout << "Parsing IN GAME BATCH #" << batch_count << std::endl;
			//std::cout << "-----------------------------------------" << std::endl;


			for (Event e: events_batch) {

				int event_type = gs.process_sensor_event(e);
				/*
				static int first_time = 0;
				if (first_time==0 && gs.is_game_active()) {
					//std::cout << "-----------------------------------------" << std::endl;
					//std::cout << "GAME HAS STARTED" << std::endl;
					//std::cout << "-----------------------------------------" << std::endl;
					first_time=1;
				}
				*/

				// Compute statistics only if the game is active and not interrutped
				if (gs.is_game_active() && !gs.is_game_interrupted() && !gs.is_during_tech_issue() && event_type == BALL_EVENT && gs.is_in_field(e.get_position())) {
					//std::cout << "-----------------------------------------" << std::endl;
					//std::cout << "-----------------------------------------" << std::endl;
					//std::cout << "FOUND BALL: (sid, ts) -> (" << e.sid << ", " << e.ts << ")" << std::endl;
					//std::cout << "-----------------------------------------" << std::endl;
					//std::cout << "-----------------------------------------" << std::endl;
					// Compute distances

					gs.compute_distances();			// Parallell operation
					//gs.print_distances();
					last_computation_ts = e.ts;

					std::pair<int, float> sid_sq_dist = gs.get_player_with_ball_poss_NEW();


					//std::cout << "Minimum distance is (sid, square distance): (" << sid_sq_dist.first << ", " << sid_sq_dist.second << ")" << std::endl;
					//std::cout << "-----------------------------------------" << std::endl;
					if (sid_sq_dist.second < square_K) {
						gs.update_possession(sid_sq_dist.first, e.ts);
					}
					else {
						////std::cout << "Not close enough... No one has the ball" << std::endl;
						gs.update_possession(NONE_PLAYER_SID, e.ts);
					}
				}

			}

			if(last_batch && gs.is_game_active()) {
				statistics.print_statistics(gs.is_game_finished());
			}

			//std::cout << "-----------------------------------------" << std::endl;
		}

	//}

	// TODO gather this in a Statistics class
	//statistics.set_console_out(true);
	//statistics.print_statistics(true);

	auto finish = std::chrono::high_resolution_clock::now();
	std::chrono::duration<double> elapsed = finish - start;
	std::cout << "Elapsed time: " << elapsed.count() << "s  Batch size " << ep.batch_size << "\n";


}
