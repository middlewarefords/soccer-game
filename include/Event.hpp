/*
 * Event.hpp
 *
 *  Created on: 19 ago 2018
 *      Author: cataldocalo
 */

#ifndef COMMAND_LINE_APP_UTILS_EVENT_HPP_
#define COMMAND_LINE_APP_UTILS_EVENT_HPP_


#include <stdio.h>
#include "Position.hpp"

/**
 * Class Event represents an event transmitted by a sensor,
 * with the relevant information according to business logic,
 * i.e. sensor id, timestamp and x, y, z coordinates for the position
 */

class Event {
public:
	int sid;					// Sensor ID
	unsigned long long ts;		// Timestamp
	int x;						// X coordinate
	int y;						// Y coordinate
	int z;						// Z coordinate

	/**
	* Constructor of an instance of Event
	* @param sid Sid of the event
	* @param ts Timestamp of the event
	* @param x X coordinate for the position of the sensor
	* @param y Y coordinate for the position of the sensor
	* @param z Z coordinate for the position of the sensor
	*/
	Event(int, unsigned long long, int, int, int);
	/**
	* Constructor of an instance of Event
	* @param sid Sid of the event
	* @param ts Timestamp of the event
	* @param pos Position of the sensor
	*/
	Event(int, unsigned long long, const Position&);
	/**
	* Constructor of an instance of Event
	* @param e The event
	*/
	Event(const Event &);

	/**
	 * @return position of current event
	 */
	Position get_position();
	/**
	 * Prints the relevant parameters of an event
	 */
	void print();
};


#endif /* COMMAND_LINE_APP_UTILS_EVENT_HPP_ */
