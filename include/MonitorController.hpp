/*
 * MonitorController.hpp
 *
 *  Created on: 22 ago 2018
 *      Author: cataldocalo
 */

#ifndef INCLUDE_MONITORCONTROLLER_HPP_
#define INCLUDE_MONITORCONTROLLER_HPP_

#include <string>
#include <unordered_map>
#include <chrono>

#include "MetadataParser.hpp"
#include "EventParser.hpp"
#include "GameState.hpp"
#include "Utility.hpp"

class MonitorController {
public:
	void run(int, int, std::string, std::string, std::string, std::string);
private:

};



#endif /* INCLUDE_MONITORCONTROLLER_HPP_ */
