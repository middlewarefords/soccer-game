/*
 * Constants.hpp
 *
 *  Created on: 02 set 2018
 *      Author: cataldocalo
 */

#ifndef INCLUDE_UTILITY_HPP_
#define INCLUDE_UTILITY_HPP_

#include <string>

const int NONE_PLAYER_SID = -2;
const int INVALID_SID = -1;
const unsigned long long INVALID_TS = 0;

const int BALL_EVENT = 2;
const int PLAYER_EVENT = 4;
const int INTERR_BEGIN_EVENT = 8;
const int INTERR_END_EVENT = 16;

const std::string NO_TEAM = "No Team";

const unsigned long long START_GAME_1ST_HALF_TS = 10753295594424116;
const unsigned long long END_GAME_1ST_HALF_TS = 12557295594424116;
const unsigned long long TECH_ISSUE_BEGIN_TS = 12398000000000000;
const unsigned long long TECH_ISSUE_END_TS = END_GAME_1ST_HALF_TS;
const unsigned long long START_GAME_2ND_HALF_TS = 13086639146403495;
const unsigned long long END_GAME_2ND_HALF_TS = 14879639146403495;

const int FIELD_LOWER_X = 0;
const int FIELD_UPPER_X = 52483;
const int FIELD_LOWER_Y = -33960;
const int FIELD_UPPER_Y = 33965;

// Converts hh:mm:ss.ms to picoseconds (hh, mm, ss, ms)
unsigned long long TO_PICO_SECONDS(int,int,int,int);

std::string TO_GAME_TIME(unsigned long long);
std::string FORMAT_DIGIT(int, int);

#endif /* INCLUDE_UTILITY_HPP_ */
