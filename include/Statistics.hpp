#ifndef INCLUDE_STATISTICS_HPP_
#define INCLUDE_STATISTICS_HPP_

#include <string>
#include <utility>
#include <fstream>
#include <iostream>
#include <algorithm>
#include <omp.h>

#include <unordered_map>
#include <map>
#include <list>

#include "Players.hpp"

class Statistics {

public:

	Statistics(std::unordered_map<int, Player*> sid_to_players, std::unordered_map<int,int>,
			   std::string, int);
	Statistics(std::map<std::string, Player*>, std::string, int);
	~Statistics();
	/**
	 * Print individual possession and teams possession
	 */
	void print_statistics(bool);
	void set_console_out(bool);


private:

  std::ofstream file;
  std::string   filename;
  bool console_out;
  int T;
  //std::map<std::string, Player*> name_to_player;
  std::vector<Player*> players;
  std::unordered_map<std::string, float> teams_possession;
  std::unordered_map<std::string, float> players_possession;
  //float total_t;
  int period;

  std::map<std::string, std::list<Player*>> teams;

  void print_to_cout(std::string);
  void print_to_file(std::string);
  void zero_stats();


};

#endif
