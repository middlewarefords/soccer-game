/*
 * EventParser.hpp
 *
 *  Created on: 19 ago 2018
 *      Author: cataldocalo
 */

#ifndef COMMAND_LINE_APP_UTILS_EVENTPARSER_HPP_
#define COMMAND_LINE_APP_UTILS_EVENTPARSER_HPP_


#include <string>
#include <vector>
#include <fstream>
#include <iostream>
#include <queue>

#include "Event.hpp"
#include "Interruption.hpp"
#include "Utility.hpp"

/**
 * EventParser is in charge of opening the stream to data file,
 * reading the events and returning them into batches.
 * It also insert interruption events into the stream of events,
 * since they are not into the same file as sensors' data file.
 */

class EventParser {
private:
	std::fstream fs;									// Stream to file
	unsigned long long T;								// Period of time reference in picoseconds
	unsigned long long time_window_start;				// Begin of period timestamp
	std::queue<Event> pending_events;					// Buffer of events to insert into batches
	std::queue<std::pair<int, unsigned long long>> interruptions; // Buffer of events to insert into batches
	/**
	 * @return first interruption event of the batch
	 */
	Event get_current_interruption();
	/**
	 * @return first interruption event of the batch
	 */
	Event get_current_event();	
	/**
	 * Make head of interruptions queue to advance
	 */
	void advance_interr_head();
	/**
	 * Make head of interruptions queue to advance
	 */
	void advance_events_head();
	/**
	 * Read next event from filestream
	 */
	void parse_next_event();
	/**
	 * @param ts Timestamp of an event
	 * @return true if @ts is during game interval, false otherwise
	 */
	bool during_game_interval(unsigned long long);
	/**
	 * @param ts Current timestamp
	 * @return true if period T is ended wrt current @ts, false otherwise
	 */
	bool is_period_ended(unsigned long long);
public:
	const int batch_size = 500;

	/**
	* Constructor of an instance of EventParser
	* @param game_data_path The game data path
	* @param T Time units, ranging from 1 to 60
	* @param start_of_the_game_ts The timestamp where the game starts
	*/
	EventParser(std::string, int, unsigned long long);
	virtual ~EventParser();

	/**
	 * @param is_last_batch It is set to true if this batch is the last of period T
	 * @return Vector of events batch
	 */
	virtual std::vector<Event> parse_window_of_event(bool&);
	bool is_eof_reached();

	/**
	 * Store the set of interruptions so that they can be inserted into the 
	 * sensor data stream. All begins are identified by a unique sid; that
	 * also holds for ends of interruptions. Each begin has to preceed its
	 * corresponding end of interruption.
	 * @param interruptions Queue of pairs <sid, timestamp> representing interruptions
	 */
	void set_interruptions(std::queue<std::pair<int, unsigned long long>>);

};



#endif /* COMMAND_LINE_APP_UTILS_EVENTPARSER_HPP_ */
