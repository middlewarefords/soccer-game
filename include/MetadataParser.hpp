/*
 * MetadataFetcher.hpp
 *
 *  Created on: 21 ago 2018
 *      Author: cataldocalo
 */

#ifndef INCLUDE_METADATAPARSER_HPP_
#define INCLUDE_METADATAPARSER_HPP_

#include <queue>
#include <regex>
#include <string>
#include <iostream>
#include <memory>
#include <unordered_map>

#include "Players.hpp"

/**
 * A MetadataParser instance reads the file "metadata.txt" and returns
 * the maps: between players and sensors worn, between players and team
 * and the set of interruptions.
 */
class MetadataParser {
public:

//sensors_file_path, std::string first_half, std::string second_half,
//							   unsigned long long offset_1st_half, unsigned long long offset_2nd_half
	/**
	 * Constructor of a MetadataParser instance
	 * @param metadata_path Path to file containing metadata information
	 * @param first_half Path to file containing metadata of 1st half of the game
	 * @param second_half Path to file containing metadata of 1st half of the game
	 * @param offset_1st_half Offset to 1st half of the game timestamp
	 * @param offset_2nd_half Offset to 2nd half of the game timestamp
	 */
	MetadataParser(std::string, std::string, std::string, unsigned long long, unsigned long long);


	/*
	 * @return The set of sensor ids associated to the balls
	 */
	std::unordered_set<int> parse_balls();
	/*
	 * @return Map Player name -> set of sensor ids
	 */
	std::unordered_map<std::string, std::unordered_set<int>> parse_players();
	/*
	 * @return Map Player name -> set of sensor ids
	 */
	std::unordered_map<std::string, std::string> parse_teams();
	/*
	 * @return Set of pairs <sid, timestamp> of interruptions
	 */
	std::queue<std::pair<int, unsigned long long>> parse_interruptions();
    /*
	 * @return Set of sids corresponding to interruption begins
	 */
	std::unordered_set<int> get_interr_begin_sids();
    /*
	 * @return Set of sids corresponding to interruption ends
	 */
	std::unordered_set<int> get_interr_end_sids();

private:
	std::string sensors_file_path;
	std::string int_fir_half_path;
	std::string int_sec_half_path;
	unsigned long long offset_1st_half;
	unsigned long long offset_2nd_half;


	bool sensors_file_parsed;
	bool interr_1sthalf_file_parsed;
	bool interr_2ndhalf_file_parsed;

	std::unordered_map<std::string, std::unordered_set<int>> players;	// Player name -> IDs of sensors worn
	std::unordered_set<int> balls;
	std::unordered_map<std::string, std::string> teams;					// Player name -> Team
	std::unordered_set<int> interr_begin;
	std::unordered_set<int> interr_end;
	std::queue<std::pair<int, unsigned long long>> interruptions;

	void parse_sensors_file();
	void parse_interruptions_file(std::string, unsigned long long);

};



#endif /* INCLUDE_METADATAPARSER_HPP_ */
