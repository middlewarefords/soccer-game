/*
 * SpecializedEventParser.hpp
 *
 *  Created on: 27 ago 2018
 *      Author: cataldocalo
 */

#ifndef INCLUDE_SPECIALIZEDEVENTPARSER_HPP_
#define INCLUDE_SPECIALIZEDEVENTPARSER_HPP_

#include "EventParser.hpp"

class SpecializedEventParser : public EventParser {
public:
	SpecializedEventParser(std::string, int, unsigned long long);
	~SpecializedEventParser() {}

	int batch_count;

	std::vector<Event> parse_window_of_event(bool&) override;

};


#endif /* INCLUDE_SPECIALIZEDEVENTPARSER_HPP_ */
