/*
 * Interruption.hpp
 *
 *  Created on: 01 set 2018
 *      Author: cataldocalo
 */

#ifndef INCLUDE_INTERRUPTION_HPP_
#define INCLUDE_INTERRUPTION_HPP_

#include "Utility.hpp"

/**
* Interruption encapsulates the begin and the end timestamps
* regarding an interruption event during the game, e.g. fouls, thrown-ins, etc.
*/

class Interruption {
private:
	unsigned long long begin_ts;		// Begin of interruption timestamp
	unsigned long long end_ts;			// End of interruption timestamp
public:
	/**
	* Default constructor which sets begin and end interruption timestamps
	* to invalid values
	*/
	Interruption();
	
	/**
	* Constructor of Interruption's instance
	* @param begin_ts Begin of interruption timestamp
	* @param end_ts End of interruption timestamp
	*/
	Interruption(unsigned long long, unsigned long long);

	/**
	* @return begin of interruption timestamp
	*/
	unsigned long long get_begin();
	/**
	* @param begin_ts Set begin of interruption timestamp
	*/
	void set_begin_ts(unsigned long long);
	/**
	* @return end of interruption timestamp
	*/
	unsigned long long get_end();
	/**
	* @param end_ts Set end of interruption timestamp
	*/
	void set_end_ts(unsigned long long);

};


#endif /* INCLUDE_INTERRUPTION_HPP_ */
