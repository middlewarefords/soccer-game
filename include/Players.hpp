#ifndef INCLUDE_PLAYERS_HPP_
#define INCLUDE_PLAYERS_HPP_

#include <fstream>
#include <iostream>
#include <regex>
#include <string>
#include <unordered_set>
#include <unordered_map>

#include "Interruption.hpp"
#include "Utility.hpp"

/**
* Player class is responsible for maintaining information about a player,
* which include the name, the set of sensor worn (identified by ID),
* and possession time of the ball. Its interface exposes methods to 
* start and end the possession with respect to a ball timestamp.
*/
class Player {
private:
	unsigned long long got_possession_ts;
	float possession_time;
public:
	std::string name;						// Name of the player
	std::string team;						// Belonging team
	/** 
	* Very hard to manage containers (vectors, unordered_set, etc.) with
	* base and derived classes. We only need two more sids for GoalKeeper
	* for the query purpose. So we just use an unordered_set of sensor IDs.
	*/
	std::unordered_set<int> sids;			//IDs of sensors worn

	//Player();
	
	/**
	* Constructor of an instance of Player
	* @param name Name of the player
	* @param sids Set of IDs of sensors worn by the player
	*/
	Player(std::string, std::unordered_set<int>);

	// This constructor is now useless
	//Player(std::string, std::string, std::unordered_set<int>);

	/**
	* Starts ball possession of the player by storing the timestamp
	* @param ball_ts Timestamp of the ball event which starts the possession
	*/
	void start_possession(unsigned long long);
	/**
	* Ends ball possession of the player by storing the timestamp
	* removing eventual game interruption happened during possession
	* @param ball_ts Timestamp of the ball event which ends the possession
	* @param interruption_to_account True if there is an interruption to account, false otherwise
	* @param interr Interruption details of the interruption
	*/
	void end_possession(unsigned long long, bool, Interruption);

	/**
	* @return Set of sensor IDs worn by the player
	*/
	std::unordered_set<int> get_sids();
	/**
	* @return Possession time of the player
	*/
	float get_possession_time();
	/**
	* @return Timestamp corresponding to the beginning of the ball possession
	*/
	unsigned long long get_got_possession_ts();
	
	/**
	* Prints to standard output a textual description of the player instance
	*/
	void print() const;
};

#endif /* INCLUDE_PLAYERS_HPP_ */
