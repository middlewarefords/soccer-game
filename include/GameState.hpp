/*
 * GameState.hpp
 *
 *  Created on: 21 ago 2018
 *      Author: cataldocalo
 */

#ifndef INCLUDE_GAMESTATE_HPP_
#define INCLUDE_GAMESTATE_HPP_


#include <unordered_map>
#include <map>
#include <unordered_set>
#include <utility>
#include <string>
#include <limits>
#include <algorithm>
#include <omp.h>
#include <cmath>

#include "Interruption.hpp"
#include "Players.hpp"
#include "Position.hpp"
#include "MonitorController.hpp"
#include "Utility.hpp"

/**
 * Class GameState is responsible of maintaining the state of the game, in particular it
 * maps the sensors (represented by Sensor ID "sid") to the entities which wear them:
 * players represented by the class Player and balls. Interruptions are represented by special
 * sids as well and taken into account when computing time possession.
 * Once an event is received, the state of the game is updated.
 */

class GameState {
public:

	GameState();
	~GameState();

	/**
	 * @param sid The sensor ID
	 * @return true if @ sid corresponds to a player, false otherwise
	 */
	bool is_player(int);
	/**
	 * @param sid The sensor ID
	 * @return true if @ sid corresponds to a ball, false otherwise
	 */
	bool is_ball(int);
	/**
	 * @param sid The sensor ID
	 * @return true if @ sid corresponds to a game interruption's begin, false otherwise
	 */
	bool is_interr_begin(int);
	/**
	 * @param sid The sensor ID
	 * @return true if @ sid corresponds to a game interruption's end, false otherwise
	 */
	bool is_interr_end(int);
	/**
	 * @return true if the game is active (1st or 2nd half), false otherwise
	 */
	bool is_game_active();
	/**
	 * @return true if the game is interrupted, false otherwise
	 */
	bool is_game_interrupted();
	/**
	 * @return true if the game is during technical issue
	 */
	bool is_during_tech_issue();
	/**
	 * @param p Position of a sensor
	 * @return true if @p is inside the field, false otherwise
	 */
	bool is_in_field(Position);

	bool is_game_finished();
	/**
	 * Add a player's reference to the map sid -> Player
	 *
	 * @param name The player's name
	 * @param sids IDs of sensors worn by @p
	 */
	void add_player(std::string, std::unordered_set<int>);
	/**
	 * Add a sid to the set of balls' sids
	 * @param sid The ball's sid
	 */
	void add_ball(int);
	/**
	 * Add team name to an already existing map of players
	 * @param player_name Name of the player
	 * @param team Name of the team
	 */
	void add_team(std::string, std::string);
	/**
	 * Add several players' references to the map sid -> Player
	 * @param Map player name to set of sids worn
	 */
	void add_players(std::unordered_map<std::string, std::unordered_set<int>>);
	/**
	 * Add team name to the players
	 * @param Map player name to team name
	 */
	void add_teams(std::unordered_map<std::string, std::string>);
	/**
	 * Add several balls' sids to the map of sid -> Position (initialized to an out of field position)
	 * @param balls_sids Set of balls' sids
	 */
	void add_balls(std::unordered_set<int>);
	/**
	 * Add several interruption begins' sids to the set of interruption begins
	 * @param balls_sids Set of balls' sids
	 */
	void add_interr_begins(std::unordered_set<int>);
	/**
	 * Add several interruption ends' sids to the set of interruption ends
	 * @param balls_sids Set of balls' sids
	 */
	void add_interr_ends(std::unordered_set<int>);

	/**
	 * @param sid Sensor ID corresponding to a player
	 * @return Player's name
	 */
	std::string get_player_name(int sid);

	/**
	 * @return the mapping name -> Player
	 */
	std::map<std::string, Player*> get_name_to_player();
	/**
	 * @return the mapping sid -> Player
	 */
	std::unordered_map<int, Player*> get_sid_to_players();

	/**
	 * Process an event depending on the entity that wears it
	 * @param e Event to be processed
	 * @return an integer representing the type of entity wearing @ e,
	 * BALL_EVENT, PLAYER_EVENT, INTERR_BEGIN_EVENT or INTERR_END_EVENT
	 */
	int process_sensor_event(Event);
	/**
	 * Compute the distances of each player to the position of the active ball
	 */
	void compute_distances();
	/**
	 * Print the distances of each player to the position of the active ball
	 */
	void print_distances();
	/**
	 * Update the position of the player with respect to the ball's event timestamp
	 * @param sid Player's sid
	 * @param ball_ts Ball's event timestamp
	 */
	void update_possession(int, unsigned long long);
	/**
	 * return The pair <sid, square_distance> corresponding to the Player in possess of the ball
	 */
	std::pair<int, float> get_player_with_ball_poss();
	std::pair<int, float> get_player_with_ball_poss_NEW();

	void print();
	void print_sid_maps();
	std::unordered_map<int, int> get_sid_to_incr_sid();
private:


	int pl_with_poss_sid;									//sid of player in possess of the ball
	std::unordered_map<int, Player*> players;				//Map player_sid -> Player
	std::unordered_map<int, Position> balls_positions;		//Map ball_sid -> Position
	std::unordered_set<int> interr_begins;					//Set of interruption begins' sids
	std::unordered_set<int> interr_ends;					//Set of interruption ends' sids

	std::unordered_map<int, Position> players_positions;	//Map player_sid -> Position
	std::unordered_map<int, float> distances;				//Map sid -> square_distance to the ball's position
	std::vector<float> sq_distances;
	std::map<std::string, Player*> name_to_player;			//Map player_name -> Player
	std::unordered_map<int, int> incr_sid_to_sid;  			//Map incremental_sid (artificial id) to sid
	std::unordered_map<int, int> sid_to_incr_sid;			//Map sid to incremental_sid (artificial id)

	bool in_game;
	int active_ball_sid;
	bool pending_interruption;
	bool interrupted_game;
	bool during_tech_issue;
	bool finished_game;

	Interruption last_interruption;							//Last interruption, still to be taken into account

	/**
	 * Initialize the distances of players from ball to zero
	 */
	void initialize_distances();
	/**
	 * Determine if game is running depending on current event timestamp
	 * @param current_ts Current timestamp
	 */
	void process_running_state(unsigned long long);
	/**
	 * Set the last interruption's begin
	 * @param begin_ts Interruption's begin timestamp
	 */
	void set_interruption_begin(unsigned long long);
	/**
	 * Set the last interruption's end
	 * @param end_ts Interruption's end timestamp
	 */
	void set_interruption_end(unsigned long long);
	/**
	 * Reset last_interruption to invalid timestamps for begin and end
	 */
	void reset_interruption();

	/**
	 * @param incr_id The incremental_sid (artificial id)
	 * @return the sid for given incr_id
	 */
	int get_sid(int);
	/**
	 * Print details of the last interruption
	 */
	void print_int();
};



#endif /* INCLUDE_GAMESTATE_HPP_ */
