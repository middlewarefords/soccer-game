/*
 * Position.hpp
 *
 *  Created on: 19 ago 2018
 *      Author: cataldocalo
 */

#ifndef COMMAND_LINE_APP_UTILS_POSITION_HPP_
#define COMMAND_LINE_APP_UTILS_POSITION_HPP_

#include "Utility.hpp"

class Position {
private:
	int x;
	int y;
	int z;

public:
	/**
	 * @return p A position which lays out the soccer field
	 */
	static Position get_out_of_field_position() {
		return Position(FIELD_LOWER_X-50, FIELD_LOWER_Y-50, 0);
	}

	/**
	* Basic constructor of an instance of Position which
	* lays out of the soccer field
	*/
	Position();
	/**
	* Constructor of an instance of Position
	* @param int x x coordinate of position
	* @param int y y coordinate of position
	* @param int z z coordinate of position
	*/
	Position(int, int, int);
	/**
	* Constructor of an instance of Position
	* @param Position p
	*/
	Position(const Position&);	// Copy constructor

	/**
	 * Operator to verify if current and given position are equal
	 * @param p Position to compare with
	 * @return True if x, y and z coordinate of given position are equal to respective
	 * x, y and z coordinate of current position, otherwise false
	 */
	Position& operator=(const Position&);
	/**
	* @return x coordinate of position
	*/
	int get_x() const;
	/**
	* @return y coordinate of position
	*/
	int get_y() const;
	/**
	* @return z coordinate of position
	*/
	int get_z() const;
	/**
	* @param p Position to calculate distance with
	* @return distance between current and given position.
	*/
	float sq_distance(const Position&) const;
};



#endif /* COMMAND_LINE_APP_UTILS_POSITION_HPP_ */
