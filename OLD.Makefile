CC=mpic++
CFLAGS= -fopenmp
OUTPUT=openmp_mpi
DEPS= utils/Interruptions.cpp utils/Interruptions.hpp utils/Players.cpp utils/Players.hpp utils/Event.cpp utils/Event.hpp utils/EventParser.cpp utils/EventParser.hpp

compile:
			$(CC) $(CFLAGS) openmp_mpi.cpp $(DEPS) -o $(OUTPUT)
