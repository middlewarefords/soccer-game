# Information about the data
http://debs.org/debs-2013-grand-challenge-soccer-monitoring/
### Download links
Data : https://aerofs.neslab.it/l/0b15ed33250349c199c37c85a7408dc9
1st Half Video: https://aerofs.neslab.it/l/a47fd7abc87b4b73b2c409d8fa18d5ad
2nd Half Video: https://aerofs.neslab.it/l/ec01789e77bc4f97a63e3f77009a30fb
https://aerofs.neslab.it/l/f7b889e2a2b74925a8e5a515b06b21a8
https://aerofs.neslab.it/l/54d2d70b25474e82845598e47f3ba7a8

# Requirements

### Compiler

g++ version 8.2 with openMP and C++11 support.

### Location of sensors data

The "full-game" file needs to be into the project directory "./".

### Location of output file

The output file "output.txt" is located after a full run into the project directory "./".

# Command line with OpenMP and MPI

### Makefile

Automatically with compile_run.sh

```sh
$ ./compile_run.sh
```
