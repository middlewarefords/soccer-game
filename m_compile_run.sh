#!/bin/bash
echo "Cleaning and compiling"
make clean
make
for i in {1..3}
do
   echo "### RUN # $i"
   ./build/apps/openmp_mpi 5 60
   sleep 5
done
