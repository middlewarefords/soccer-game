#!/bin/bash
# Compile
# mpic++ -fopenmp openmp_mpi.cpp utils/Interruptions.cpp utils/Interruptions.hpp -o openmp_mpi
make
# Run K and T are the last two numbers
./build/apps/openmp_mpi 5 60

